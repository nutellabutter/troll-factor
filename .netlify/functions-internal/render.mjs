import { init } from '../serverless.js';

export const handler = init({
	appDir: "_app",
	appPath: "_app",
	assets: new Set([".DS_Store","bg.mp3","click.mp3","favicon.png","menu.mp3","styles.css","troll.mp3","troll.png"]),
	mimeTypes: {".mp3":"audio/mpeg",".png":"image/png",".css":"text/css"},
	_: {
		entry: {"file":"_app/immutable/start-fa1414bc.js","imports":["_app/immutable/start-fa1414bc.js","_app/immutable/chunks/index-7e6c99cc.js","_app/immutable/chunks/singletons-513f472a.js","_app/immutable/chunks/index-3cdda9ac.js"],"stylesheets":[]},
		nodes: [
			() => import('../server/nodes/0.js'),
			() => import('../server/nodes/1.js'),
			() => import('../server/nodes/2.js'),
			() => import('../server/nodes/3.js')
		],
		routes: [
			{
				id: "/",
				pattern: /^\/$/,
				names: [],
				types: [],
				optional: [],
				page: { layouts: [0], errors: [1], leaf: 2 },
				endpoint: null
			},
			{
				id: "/decryptScore",
				pattern: /^\/decryptScore\/?$/,
				names: [],
				types: [],
				optional: [],
				page: null,
				endpoint: () => import('../server/entries/endpoints/decryptScore/_server.js')
			},
			{
				id: "/encryptScore",
				pattern: /^\/encryptScore\/?$/,
				names: [],
				types: [],
				optional: [],
				page: null,
				endpoint: () => import('../server/entries/endpoints/encryptScore/_server.js')
			},
			{
				id: "/test",
				pattern: /^\/test\/?$/,
				names: [],
				types: [],
				optional: [],
				page: { layouts: [0], errors: [1], leaf: 3 },
				endpoint: () => import('../server/entries/endpoints/test/_server.js')
			},
			{
				id: "/updateHighScore",
				pattern: /^\/updateHighScore\/?$/,
				names: [],
				types: [],
				optional: [],
				page: null,
				endpoint: () => import('../server/entries/endpoints/updateHighScore/_server.js')
			},
			{
				id: "/updatePlayerScore",
				pattern: /^\/updatePlayerScore\/?$/,
				names: [],
				types: [],
				optional: [],
				page: null,
				endpoint: () => import('../server/entries/endpoints/updatePlayerScore/_server.js')
			}
		],
		matchers: async () => {
			
			return {  };
		}
	}
});
