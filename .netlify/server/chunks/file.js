import { S3Client, GetObjectCommand } from "@aws-sdk/client-s3";
import { Upload } from "@aws-sdk/lib-storage";
import "dotenv";
const s3Client = new S3Client({
  region: "us-east-1",
  accessKeyId: process.env.AWS_A_KEY_ID,
  secretAccessKey: process.env.AWS_S_ACCESS_KEY
});
async function getContents(Key) {
  const s3Command = new GetObjectCommand({ Bucket: "trollfactor", Key });
  const s3Response = await s3Client.send(s3Command);
  const fetchResponse = new Response(s3Response.Body);
  const contents = await fetchResponse.json();
  return contents;
}
async function uploadFile(blob, Key) {
  const upload = new Upload({
    client: s3Client,
    params: { Bucket: "trollfactor", Key, Body: blob }
  });
  await upload.done();
}
export {
  getContents as g,
  uploadFile as u
};
