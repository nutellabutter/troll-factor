import { w as writable } from "./index2.js";
const scorePlayer = writable(0);
const scoreTroll = writable(0);
const nButtons = writable(0);
const isAnimating = writable(false);
const state = writable("");
const mode = writable("menu");
const story = writable(false);
const highscore = writable();
const showScoreInput = writable(false);
const newHighScore = writable();
const showHighScore = writable(false);
export {
  state as a,
  scoreTroll as b,
  showScoreInput as c,
  nButtons as d,
  showHighScore as e,
  story as f,
  highscore as h,
  isAnimating as i,
  mode as m,
  newHighScore as n,
  scorePlayer as s
};
