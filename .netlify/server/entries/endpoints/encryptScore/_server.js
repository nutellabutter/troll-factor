import { e as encrypt } from "../../../chunks/crypto.js";
async function POST({ request }) {
  const payload = await request.json();
  return new Response(
    JSON.stringify({
      scorePlayer: encrypt(payload.value.toString())
    }),
    {
      status: 200
    }
  );
}
export {
  POST
};
