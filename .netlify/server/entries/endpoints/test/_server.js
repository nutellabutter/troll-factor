import { u as uploadFile } from "../../../chunks/file.js";
async function POST({ request }) {
  const payload = await request.json();
  await uploadFile(payload.Body, payload.Key);
  return new Response(
    JSON.stringify({
      message: "Score updated"
    }),
    {
      status: 200
    }
  );
}
export {
  POST
};
