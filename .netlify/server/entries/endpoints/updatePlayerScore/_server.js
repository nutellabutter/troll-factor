import { e as encrypt } from "../../../chunks/crypto.js";
import { s as scorePlayer } from "../../../chunks/store.js";
import { g as get_store_value } from "../../../chunks/index.js";
async function POST({ request }) {
  const payload = await request.json();
  const updatedScore = get_store_value(scorePlayer) + payload.value;
  return new Response(
    JSON.stringify({
      scorePlayer: encrypt(updatedScore.toString())
    }),
    {
      status: 200
    }
  );
}
export {
  POST
};
