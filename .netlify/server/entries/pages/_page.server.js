import { g as getContents } from "../../chunks/file.js";
async function load({ params }) {
  const highscore = await getContents("highscore.json");
  return highscore;
}
export {
  load
};
