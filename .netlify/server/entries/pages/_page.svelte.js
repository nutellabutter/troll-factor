import { c as create_ssr_component, d as subscribe, e as escape, f as add_attribute, v as validate_component, h as each } from "../../chunks/index.js";
import { a as state, i as isAnimating, b as scoreTroll, s as scorePlayer, c as showScoreInput, n as newHighScore, m as mode, d as nButtons, e as showHighScore, f as story, h as highscore } from "../../chunks/store.js";
const ButtonNumber_svelte_svelte_type_style_lang = "";
const css = {
  code: ".button.svelte-u08qy9{width:3rem}",
  map: null
};
const ButtonNumber = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$unsubscribe_state;
  let $$unsubscribe_isAnimating;
  let $$unsubscribe_scoreTroll;
  let $$unsubscribe_scorePlayer;
  let $$unsubscribe_showScoreInput;
  let $$unsubscribe_newHighScore;
  $$unsubscribe_state = subscribe(state, (value2) => value2);
  $$unsubscribe_isAnimating = subscribe(isAnimating, (value2) => value2);
  $$unsubscribe_scoreTroll = subscribe(scoreTroll, (value2) => value2);
  $$unsubscribe_scorePlayer = subscribe(scorePlayer, (value2) => value2);
  $$unsubscribe_showScoreInput = subscribe(showScoreInput, (value2) => value2);
  $$unsubscribe_newHighScore = subscribe(newHighScore, (value2) => value2);
  let { value } = $$props;
  let { visibility = "visible" } = $$props;
  let button;
  if ($$props.value === void 0 && $$bindings.value && value !== void 0)
    $$bindings.value(value);
  if ($$props.visibility === void 0 && $$bindings.visibility && visibility !== void 0)
    $$bindings.visibility(visibility);
  $$result.css.add(css);
  $$unsubscribe_state();
  $$unsubscribe_isAnimating();
  $$unsubscribe_scoreTroll();
  $$unsubscribe_scorePlayer();
  $$unsubscribe_showScoreInput();
  $$unsubscribe_newHighScore();
  return `<button class="${"is-dark button svelte-u08qy9"}" style="${"visibility:" + escape(visibility, true) + ";"}"${add_attribute("this", button, 0)}>${escape(value + 1)}
</button>`;
});
const ButtonDifficulty = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$unsubscribe_state;
  let $$unsubscribe_mode;
  let $$unsubscribe_nButtons;
  let $$unsubscribe_scoreTroll;
  let $$unsubscribe_scorePlayer;
  $$unsubscribe_state = subscribe(state, (value) => value);
  $$unsubscribe_mode = subscribe(mode, (value) => value);
  $$unsubscribe_nButtons = subscribe(nButtons, (value) => value);
  $$unsubscribe_scoreTroll = subscribe(scoreTroll, (value) => value);
  $$unsubscribe_scorePlayer = subscribe(scorePlayer, (value) => value);
  let { difficulty } = $$props;
  let { color } = $$props;
  if ($$props.difficulty === void 0 && $$bindings.difficulty && difficulty !== void 0)
    $$bindings.difficulty(difficulty);
  if ($$props.color === void 0 && $$bindings.color && color !== void 0)
    $$bindings.color(color);
  $$unsubscribe_state();
  $$unsubscribe_mode();
  $$unsubscribe_nButtons();
  $$unsubscribe_scoreTroll();
  $$unsubscribe_scorePlayer();
  return `<button class="${"is-small button " + escape(color, true)}">${escape(difficulty)}</button>`;
});
const ButtonSubmitScore = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$unsubscribe_newHighScore;
  let $$unsubscribe_scorePlayer;
  let $$unsubscribe_showScoreInput;
  $$unsubscribe_newHighScore = subscribe(newHighScore, (value) => value);
  $$unsubscribe_scorePlayer = subscribe(scorePlayer, (value) => value);
  $$unsubscribe_showScoreInput = subscribe(showScoreInput, (value) => value);
  let { playerName } = $$props;
  if ($$props.playerName === void 0 && $$bindings.playerName && playerName !== void 0)
    $$bindings.playerName(playerName);
  $$unsubscribe_newHighScore();
  $$unsubscribe_scorePlayer();
  $$unsubscribe_showScoreInput();
  return `<button class="${"is-small button is-dark"}">Submit
</button>`;
});
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $showHighScore, $$unsubscribe_showHighScore;
  let $story, $$unsubscribe_story;
  let $showScoreInput, $$unsubscribe_showScoreInput;
  let $mode, $$unsubscribe_mode;
  let $highscore, $$unsubscribe_highscore;
  let $scoreTroll, $$unsubscribe_scoreTroll;
  let $scorePlayer, $$unsubscribe_scorePlayer;
  let $nButtons, $$unsubscribe_nButtons;
  let $state, $$unsubscribe_state;
  let $isAnimating, $$unsubscribe_isAnimating;
  $$unsubscribe_showHighScore = subscribe(showHighScore, (value) => $showHighScore = value);
  $$unsubscribe_story = subscribe(story, (value) => $story = value);
  $$unsubscribe_showScoreInput = subscribe(showScoreInput, (value) => $showScoreInput = value);
  $$unsubscribe_mode = subscribe(mode, (value) => $mode = value);
  $$unsubscribe_highscore = subscribe(highscore, (value) => $highscore = value);
  $$unsubscribe_scoreTroll = subscribe(scoreTroll, (value) => $scoreTroll = value);
  $$unsubscribe_scorePlayer = subscribe(scorePlayer, (value) => $scorePlayer = value);
  $$unsubscribe_nButtons = subscribe(nButtons, (value) => $nButtons = value);
  $$unsubscribe_state = subscribe(state, (value) => $state = value);
  $$unsubscribe_isAnimating = subscribe(isAnimating, (value) => $isAnimating = value);
  let { data } = $$props;
  let playerName;
  highscore.set(data);
  if ($$props.data === void 0 && $$bindings.data && data !== void 0)
    $$bindings.data(data);
  $$unsubscribe_showHighScore();
  $$unsubscribe_story();
  $$unsubscribe_showScoreInput();
  $$unsubscribe_mode();
  $$unsubscribe_highscore();
  $$unsubscribe_scoreTroll();
  $$unsubscribe_scorePlayer();
  $$unsubscribe_nButtons();
  $$unsubscribe_state();
  $$unsubscribe_isAnimating();
  return `<div class="${"section"}"><div class="${"container"}"><div class="${"columns has-text-centered is-centered"}"><h1 class="${"title"}">TROLL FACTOR</h1></div></div></div>

${$mode === "menu" ? `<div class="${"section"}"><div class="${"container"}"><div class="${"columns has-text-centered is-centered"}"><img width="${"250px"}" alt="${"A troll"}" src="${"troll.png"}"></div></div></div>

    <div class="${"section"}"><div class="${"container"}"><div class="${"columns buttons is-centered"}">${validate_component(ButtonDifficulty, "ButtonDifficulty").$$render($$result, { difficulty: "Noob", color: "is-primary" }, {}, {})}
                ${validate_component(ButtonDifficulty, "ButtonDifficulty").$$render($$result, { difficulty: "Easy", color: "is-success" }, {}, {})}
                ${validate_component(ButtonDifficulty, "ButtonDifficulty").$$render(
    $$result,
    {
      difficulty: "Normal",
      color: "is-warning"
    },
    {},
    {}
  )}
                ${validate_component(ButtonDifficulty, "ButtonDifficulty").$$render($$result, { difficulty: "Hard", color: "is-danger" }, {}, {})}</div>
            <div class="${"columns buttons is-centered"}"><button class="${"button is-small is-info"}">${$story ? `Hide` : `Show`} Story</button>
                <button class="${"button is-small is-info"}">${$showHighScore ? `Hide` : `Show`} High Score</button></div></div></div>

    ${$showHighScore ? `<div class="${"section"}"><div class="${"container has-text-centered"}"><div class="${"block"}"><h2 class="${"subtitle is-3"}">HIGH SCORE</h2></div>
            <table class="${"table is-bordered is-narrow is-fullwidth"}"><thead><th class="${"has-text-centered"}">Name</th>
                    <th class="${"has-text-centered"}">Score</th></thead>
                ${each($highscore.highscore, (list, index) => {
    return `<tr><td class="${"has-text-centered"}">${escape(list.name)}</td>
                    <td class="${"has-text-centered"}">${escape(list.score)}</td>
                </tr>`;
  })}</table></div></div>` : ``} 

    ${$story ? `<div class="${"section"}"><div class="${"container has-text-centered"}"><div class="${"block"}"><h2 class="${"subtitle is-3"}">STORY</h2></div>
                <div class="${"block"}">In the year 2007, Russian military intelligence analysts reported that one of the United States most secretive spy satellites the KH-13 was downed in the South american nation of Peru.</div>
                <div class="${"block"}">The KH-13 carried hazardous materials on board.</div>
                <div class="${"block"}">Hundreds were reported ill from radiation poisoning.</div>
                <div class="${"block"}">The mainstream press disavowed any knowledge.</div>        
                <div class="${"block"}">The Peruvian government assembled a top secret team to the crash site.</div>
                <div class="${"block"}">Only one member made it back alive.</div>
                <div class="${"block"}">He brought troubling news. Disturbing truth.</div>
                <div class="${"block"}">Are you ready for the truth?</div>
                <div class="${"block"}">I hope you are. It&#39;s not going to be easy.</div>
                <div class="${"block"}">Mutations occurred.</div>
                <div class="${"block"}">Those who were ill had developed a condition.</div>
                <div class="${"block"}">They gained superhuman strength, lost most of their hair, their skin thickened and became a dirty green hue.</div>
                <div class="${"block"}">Those who had seen them called them Trolls.</div>
                <div class="${"block"}">The Trolls lived peacefully among the local Peruvian tribes.</div>
                <div class="${"block"}">Until one day...</div>
                <div class="${"block"}">The Trolls bought iPads, picked up programming langauges and created a game on their iPad.</div>
                <div class="${"block"}">One day a Troll approached a young tribesman with an iPad and challenged him to a game.</div>
                <div class="${"block"}">The young tribesman lost the game got owned by the Troll.</div>
                <div class="${"block"}">The Troll ate him up.</div>
                <div class="${"block"}">Thousands of tribal people fled.</div>
                <div class="${"block"}">Many years later a top secret project was launched by the Peruvian government.</div>
                <div class="${"block"}">Their mission...</div>
                <div class="${"block"}">To hack into the elaborate and extensive Troll network infrastructure and retrieve the source code of the Troll game.</div>
                <div class="${"block"}">They found it.</div>
                <div class="${"block"}">It was called...</div>
                <div class="${"block"}">TROLL FACTOR</div>
                <div class="${"block"}">That&#39;s right.</div>
                <div class="${"block"}">Pretty unbelievable huh.</div>        
                <div class="${"block"}">They&#39;re all around us.</div>        
                <div class="${"block"}">The Trolls.</div>        
                <div class="${"block"}">Now is your chance, to practice this game...</div>        
                <div class="${"block"}">Before one of them approaches you\u2026</div>        
                <div class="${"block"}">When you least expected...</div>
                <div class="${"block"}">Cheers.</div></div></div>` : ``}` : `<div class="${"section"}"><div class="${"container"}"><p>Troll: ${escape($scoreTroll)}</p>
                <p>Player: ${escape($scorePlayer)}</p></div></div>
        
        <div class="${"section"}"><div class="${"container"}"><div class="${"is-multiline columns buttons"}">${each(Array($nButtons), (_, index) => {
    return `${validate_component(ButtonNumber, "Button").$$render($$result, { value: index }, {}, {})}`;
  })}</div></div></div>
        
        <div class="${"section"}"><div class="${"container"}"><p>${escape($state)}</p></div></div>

        ${$showScoreInput ? `<div class="${"section"}"><div class="${"container"}"><div class="${"block"}"><p>NEW HIGH SCORE</p></div>
                <input class="${"input"}" type="${"text"}" placeholder="${"Name"}"${add_attribute("value", playerName, 0)}>
                ${validate_component(ButtonSubmitScore, "ButtonSubmitScore").$$render($$result, { playerName }, {}, {})}</div></div>` : ``}
        
        <div class="${"section"}"><div class="${"container"}"><div class="${"columns is-centered"}">${!$isAnimating ? `<button class="${"button is-info is-small"}">Back</button>` : ``}</div></div></div>`}

<footer class="${"footer"}"><div class="${"content has-text-centered is-small"}"><p>Adapted from <a href="${"https://www.youtube.com/watch?v=Cn7VsB_N2Qc"}">Tax Factor</a>. Open sourced on <a href="${"https://gitlab.com/nutellabutter/troll-factor"}">Gitlab</a>.</p>
        <p>Built with \u2764\uFE0F for my triple J&#39;s on <a href="${"https://svelte.dev"}">Svelte</a>.</p>
        <p>Because you asked. Love you always.</p>
        <audio controls autoplay loop><source src="${"./bg.mp3"}" type="${"audio/mpeg"}"></audio></div></footer>`;
});
export {
  Page as default
};
