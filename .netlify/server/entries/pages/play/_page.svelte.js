import { c as create_ssr_component, b as subscribe, e as escape, d as add_attribute, f as set_store_value, h as each, v as validate_component } from "../../../chunks/index.js";
import { b as state, i as isAnimating, s as scoreTroll, a as scorePlayer, n as nButtons, d as difficulty } from "../../../chunks/store.js";
const Button = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$unsubscribe_state;
  let $$unsubscribe_isAnimating;
  let $$unsubscribe_scoreTroll;
  let $$unsubscribe_scorePlayer;
  $$unsubscribe_state = subscribe(state, (value2) => value2);
  $$unsubscribe_isAnimating = subscribe(isAnimating, (value2) => value2);
  $$unsubscribe_scoreTroll = subscribe(scoreTroll, (value2) => value2);
  $$unsubscribe_scorePlayer = subscribe(scorePlayer, (value2) => value2);
  let { value } = $$props;
  let { visibility = "visible" } = $$props;
  let button;
  if ($$props.value === void 0 && $$bindings.value && value !== void 0)
    $$bindings.value(value);
  if ($$props.visibility === void 0 && $$bindings.visibility && visibility !== void 0)
    $$bindings.visibility(visibility);
  $$unsubscribe_state();
  $$unsubscribe_isAnimating();
  $$unsubscribe_scoreTroll();
  $$unsubscribe_scorePlayer();
  return `<button class="${"is-dark button"}" style="${"visibility:" + escape(visibility, true) + ";"}"${add_attribute("this", button, 0)}>${escape(value + 1)}</button>`;
});
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $nButtons, $$unsubscribe_nButtons;
  let $$unsubscribe_difficulty;
  let $state, $$unsubscribe_state;
  let $scoreTroll, $$unsubscribe_scoreTroll;
  let $scorePlayer, $$unsubscribe_scorePlayer;
  let $isAnimating, $$unsubscribe_isAnimating;
  $$unsubscribe_nButtons = subscribe(nButtons, (value) => $nButtons = value);
  $$unsubscribe_difficulty = subscribe(difficulty, (value) => value);
  $$unsubscribe_state = subscribe(state, (value) => $state = value);
  $$unsubscribe_scoreTroll = subscribe(scoreTroll, (value) => $scoreTroll = value);
  $$unsubscribe_scorePlayer = subscribe(scorePlayer, (value) => $scorePlayer = value);
  $$unsubscribe_isAnimating = subscribe(isAnimating, (value) => $isAnimating = value);
  set_store_value(state, $state = "[Troll] Pick a number.", $state);
  $$unsubscribe_nButtons();
  $$unsubscribe_difficulty();
  $$unsubscribe_state();
  $$unsubscribe_scoreTroll();
  $$unsubscribe_scorePlayer();
  $$unsubscribe_isAnimating();
  return `<div class="${"section"}"><div class="${"container"}"><p>Troll: ${escape($scoreTroll)}</p>
        <p>Player: ${escape($scorePlayer)}</p></div></div>

<div class="${"section"}"><div class="${"container"}"><div class="${"is-multiline columns buttons"}">${each(Array($nButtons), (_, index) => {
    return `${validate_component(Button, "Button").$$render($$result, { value: index }, {}, {})}`;
  })}</div></div></div>

<div class="${"section"}"><div class="${"container"}"><p>${escape($state)}</p></div></div>

<div class="${"section"}"><div class="${"container"}">${!$isAnimating ? `<a href="${"../"}" class="${"button is-dark is-small"}">Back</a>` : ``}</div></div>`;
});
export {
  Page as default
};
