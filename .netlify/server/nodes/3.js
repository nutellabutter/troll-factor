import * as server from '../entries/pages/test/_page.server.js';

export const index = 3;
export const component = async () => (await import('../entries/pages/test/_page.svelte.js')).default;
export const file = '_app/immutable/components/pages/test/_page.svelte-5234a3c5.js';
export { server };
export const imports = ["_app/immutable/components/pages/test/_page.svelte-5234a3c5.js","_app/immutable/chunks/index-7e6c99cc.js","_app/immutable/chunks/store-c726b7fd.js","_app/immutable/chunks/index-3cdda9ac.js"];
export const stylesheets = [];
