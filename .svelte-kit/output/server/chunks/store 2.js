import { w as writable } from "./index2.js";
const scorePlayer = writable(0);
const scoreTroll = writable(0);
const difficulty = writable(0);
const nButtons = writable(0);
const isAnimating = writable(false);
const state = writable("");
export {
  scorePlayer as a,
  state as b,
  difficulty as d,
  isAnimating as i,
  nButtons as n,
  scoreTroll as s
};
