import { c as create_ssr_component, b as subscribe } from "../../chunks/index.js";
import { s as scoreTroll, a as scorePlayer } from "../../chunks/store.js";
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $$unsubscribe_scoreTroll;
  let $$unsubscribe_scorePlayer;
  $$unsubscribe_scoreTroll = subscribe(scoreTroll, (value) => value);
  $$unsubscribe_scorePlayer = subscribe(scorePlayer, (value) => value);
  $$unsubscribe_scoreTroll();
  $$unsubscribe_scorePlayer();
  return `<div class="${"section"}"><div class="${"container"}"><div class="${"columns is-centered"}"><h1 class="${"title is-1 is-centered"}">Troll Factor</h1></div></div></div>

<div class="${"section"}"><div class="${"container"}"><div class="${"columns is-centered"}"><img alt="${"A troll"}" src="${"troll.png"}"></div></div></div>

<div class="${"section"}"><div class="${"container"}"><div class="${"columns buttons is-centered"}"><a href="${"/play"}" class="${"button is-dark"}">Easy</a>
            <a href="${"/play"}" class="${"button is-dark"}">Medium</a>
            <a href="${"/play"}" class="${"button is-dark"}">Hard</a></div></div></div>`;
});
export {
  Page as default
};
