import { c as create_ssr_component, d as subscribe, h as each, f as add_attribute, e as escape } from "../../../chunks/index.js";
import { h as highscore } from "../../../chunks/store.js";
let sScore = 117;
const Page = create_ssr_component(($$result, $$props, $$bindings, slots) => {
  let $highscore, $$unsubscribe_highscore;
  $$unsubscribe_highscore = subscribe(highscore, (value) => $highscore = value);
  let { data } = $$props;
  let playerName;
  let i;
  highscore.set(data);
  let hasNewHighScore = false;
  let newHighScore = JSON.parse(JSON.stringify(data));
  newHighScore.highscore = newHighScore.highscore.sort((a, b) => b.score - a.score);
  for (i = 0; i < newHighScore.highscore.length; i++) {
    if (sScore > newHighScore.highscore[i].score) {
      hasNewHighScore = true;
      break;
    }
  }
  if (hasNewHighScore) {
    console.log("is new high");
  } else {
    console.log("ok");
  }
  if ($$props.data === void 0 && $$bindings.data && data !== void 0)
    $$bindings.data(data);
  $$unsubscribe_highscore();
  return `<table class="${"table is-bordered"}"><thead><th>Name</th>
        <th>Score</th></thead>
    ${each($highscore.highscore, (list, index) => {
    return `<tr><td>${escape(list.name)}</td>
        <td>${escape(list.score)}</td>
    </tr>`;
  })}</table>

<input class="${"input"}" type="${"text"}" placeholder="${"Name"}"${add_attribute("value", playerName, 0)}>
<button class="${"button"}">Submit</button>`;
});
export {
  Page as default
};
