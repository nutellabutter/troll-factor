import * as server from '../entries/pages/_page.server.js';

export const index = 2;
export const component = async () => (await import('../entries/pages/_page.svelte.js')).default;
export const file = '_app/immutable/components/pages/_page.svelte-da3b31d9.js';
export { server };
export const imports = ["_app/immutable/components/pages/_page.svelte-da3b31d9.js","_app/immutable/chunks/index-7e6c99cc.js","_app/immutable/chunks/store-c726b7fd.js","_app/immutable/chunks/index-3cdda9ac.js"];
export const stylesheets = ["_app/immutable/assets/_page-6f501642.css"];
