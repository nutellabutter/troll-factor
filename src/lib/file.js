import {
  S3Client,
  GetObjectCommand,
  DeleteObjectCommand,
} from "@aws-sdk/client-s3";
import { Upload } from "@aws-sdk/lib-storage";
import dotenv from "dotenv";

dotenv.config();

const s3Client = new S3Client({
  region: "us-east-1",
  credentials: {
    accessKeyId: process.env["AWS_A_KEY_ID"],
    secretAccessKey: process.env["AWS_S_ACCESS_KEY"],
  },
});

export async function getContents(Key) {
  const s3Command = new GetObjectCommand({ Bucket: "trollfactor", Key });
  const s3Response = await s3Client.send(s3Command);
  const fetchResponse = new Response(s3Response.Body);
  const contents = await fetchResponse.json();
  return contents;
}

export async function uploadFile(blob, Key) {
  const upload = new Upload({
    client: s3Client,
    params: { Bucket: "trollfactor", Key, Body: blob },
  });
  await upload.done();
}

export async function deleteFile(Key) {
  try {
    console.log(Key);
    const s3Command = new DeleteObjectCommand({ Bucket: "trollfactor", Key });
    await s3Client.send(s3Command);
  } catch (e) {
    console.log(e);
  }
}
