import { writable } from 'svelte/store'

export const scorePlayer = writable(0);
export const scorePlayerEncrypted = writable('');
export const scoreTroll = writable(0);
export const nButtons = writable(0);
export const isAnimating = writable(false);
export const state = writable('');
export const mode = writable('menu');
export const story = writable(false);
export const instructions = writable(false);
export const highscore = writable();
export const showScoreInput = writable(false);
export const newHighScore = writable();
export const showHighScore = writable(false);


