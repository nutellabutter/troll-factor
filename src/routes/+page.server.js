import { getContents } from "$lib/file";

export async function load({ params }) {
  const highscore = await getContents("highscore.json");
  return highscore;
}
