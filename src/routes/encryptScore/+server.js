import { encrypt } from "$lib/crypto";

export async function POST({ request }) {
  const payload = await request.json();

  return new Response(
    JSON.stringify({
      scorePlayer: encrypt(payload.value.toString()),
    }),
    {
      status: 200,
    }
  );
}
w