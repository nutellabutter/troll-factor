import { uploadFile } from "$lib/file";
export async function POST({ request }) {
  const payload = await request.json();

  const req = await uploadFile(payload.Body, payload.Key);

  return new Response(
    JSON.stringify({
      message: "High score updated",
    }),
    {
      status: 200,
    }
  );
}
