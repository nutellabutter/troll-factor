import { encrypt } from "$lib/crypto";
import { scorePlayer } from "$lib/store.js";
import { get } from "svelte/store";

export async function POST({ request }) {
  const payload = await request.json();

  const updatedScore = get(scorePlayer) + payload.value;

  return new Response(
    JSON.stringify({
      scorePlayer: encrypt(updatedScore.toString()),
    }),
    {
      status: 200,
    }
  );
}
